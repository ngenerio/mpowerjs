;(function () {
  function createHTML(obj) {
    var html = ['<div class="mp-cart-items" data-name="' + obj.name + '" data-description="It is a' + obj.name + '" data-price="' + Number(obj.price.split(',').join('')) + '">',
                  '<div class="product">',
                    '<img class="product-img" src="' + obj.img + ' " alt="' + obj.name + '" />',
                    '<p class="product-name">' + obj.name + '</p>',
                    '<p class="product-price">GHS ' + obj.price + '</p>',
                  '</div>',
                  '<button class="mp-add-cart">Add to Cart</button>',
                '</div>'
                ].join('')

    MPWidget.$('.app-container .items-holder').append(html)
  }

  var objJSON = {
    "items": [
      {
        "name": "Fresh Tab",
        "img": "//i.tonaton-st.com/b8b30889-a465-4f27-a555-64cc032aeb38/320/240/cropped.jpg",
        "price": "280"
      },
      {
        "name": "Brand new Asus",
        "img": "//i.tonaton-st.com/363e06f8-f1f7-4ab5-b064-e18e4df2febc/320/240/cropped.jpg",
        "price": "2,200"
      },
      {
        "name": "Top rated Samsung",
        "img": "//i.tonaton-st.com/5392fd11-8fd3-4936-b188-fd2370e316dd/272/204/cropped.jpg",
        "price": "5,999"
      },
      {
        "name": "Volkswagen",
        "img": "//i.tonaton-st.com/26d61c0f-178d-47a4-bc47-bd9033479770/272/204/cropped.jpg",
        "price": "11,000"
      },
      {
        "name": "Blackberry BlackBerry Bold 5 Original",
        "img": "//i.tonaton-st.com/6e8f0889-b343-4d52-94c5-a288a45566ca/272/204/cropped.jpg",
        "price": "300"
      },
      {
        "name": "Samsung Galaxy s4 active Original",
        "img": "//i.tonaton-st.com/9e6c45ed-a5ef-4049-b257-68c371b9d4f6/272/204/cropped.jpg",
        "price": "600"
      },
      {
        "name": "Mercedes Benz c300 4matic 2009",
        "img": "//i.tonaton-st.com/5e9900ea-8a1d-4acf-9e14-2866f830eb06/272/204/cropped.jpg",
        "price": "75,000"
      },
      {
        "name": "LG G3 Original",
        "img": "//i.tonaton-st.com/0aab7f35-264e-4dbb-92ce-47c477d12d44/272/204/cropped.jpg",
        "price": "1,450"
      },
      {
        "name": "Genuine Samsung 40in 3D Smart LedTv",
        "img": "//i.tonaton-st.com/0a1aa070-50b5-424f-806c-a94dc7e63462/272/204/cropped.jpg",
        "price": "2,600"
      },
      {
        "name": "Raleigh cycle",
        "img": "//i.tonaton-st.com/f1581acf-db26-41e7-bf51-1f08822728ed/272/204/cropped.jpg",
        "price": "200"
      },
      {
        "name": "Asus Mini for sale",
        "img": "//i.tonaton-st.com/3abe4a6a-9316-440f-9a5d-d7fce610d772/272/204/cropped.jpg",
        "price": "750"
      },
      {
        "name": "Fresh Samsung Fridge",
        "img": "//i.tonaton-st.com/31b796bf-a901-4161-a825-fc9fd8f2959d/272/204/cropped.jpg",
        "price": "2,600"
      },
      {
        "name": "New Samsung Air Conditioner",
        "img": "//i.tonaton-st.com/36c36038-22b4-4306-9070-db2b2d5ca85c/272/204/cropped.jpg",
        "price": "2,990"
      },
      {
        "name": "Mercedes Benz GL450 4matic 2012",
        "img": "//i.tonaton-st.com/f5f5b7bb-f51f-4f10-bc76-0673a350c043/272/204/cropped.jpg",
        "price": "177"
      },
      {
        "name": "MIDEA 180LTR DEFROST FRIDGE",
        "img": "//i.tonaton-st.com/fff658f9-a101-45f4-9ec6-eace793dd790/272/204/cropped.jpg",
        "price": "1,380"
      },
      {
        "name": "Itel 1701 Original",
        "img": "//i.tonaton-st.com/30145481-2951-467b-b465-65e11cf0968c/272/204/cropped.jpg",
        "price": "400"
      },
      {
        "name": "HP ENVY core i7 12G RAM 1TB HDD 15.6INCH",
        "img": "//i.tonaton-st.com/4b44168c-4344-4c69-81d6-7bbdea7c2306/272/204/cropped.jpg",
        "price": "2,200"
      },
      {
        "name": "Samsung samsung s4 Original",
        "img": "//i.tonaton-st.com/5d39f933-89ed-45d4-8c9d-52b26ae4728a/272/204/cropped.jpg",
        "price": "800"
      },
      {
        "name": "genius system bluetooth",
        "img": "//i.tonaton-st.com/ccbb2c67-8aef-4b66-8949-ff4951c017df/272/204/cropped.jpg",
        "price": "1,399"
      },
      {
        "name": "A home used Amp",
        "img": "images/default.png",
        "price": "250"
      },
      {
        "name": "Robust OCEAN Washing Machine",
        "img": "images/default.png",
        "price": "650"
      },
      {
        "name": "Volvo S60 T5 2014",
        "img": "//i.tonaton-st.com/d6061d6e-8f75-4fdf-82ab-52b7a2ba8672/272/204/cropped.jpg",
        "price": "215,594"
      },
      {
        "name": "NASCO 2.0HP Split A.C (MS12F-18CR-1)",
        "img": "//i.tonaton-st.com/ac300412-583d-4631-8dc4-ccf26f101aa1/272/204/cropped.jpg",
        "price": "1,900"
      },
      {
        "name": "Top Rated LG Sat TV",
        "img": "//i.tonaton-st.com/ff20cdef-4015-48c4-9dc9-d4aca1273f7f/272/204/cropped.jpg",
        "price": "3,999"
      }
    ]
  };


  for (var i = 0, l =  objJSON.items.length; i < l; i++) createHTML(objJSON.items[i])
  MPWidget.$('.app-container .items-holder').append('<button class="mp-view-cart">View Cart</button>')
  new MPWidget({
    setup: {
      masterKey: '55647970-22e1-4e7e-8fb4-56eca2b3b006',
      privateKey: 'test_private_B8EiE1AGWpb4tVMzVTyFDu9rYoc',
      publicKey: 'test_public_B1wo2UVmxUrvwzZuPqpLrWqlA74',
      token: 'a6d96e2586c8bbae7c28',
      mode: 'test'
    },
    store: {
      name: 'Awesome Store', // only name is required
      tagline: 'Easy shopping',
      phoneNumber: '0272271893',
      postalAddress: 'P.0. Box MP555, Accra',
      logoURL: 'http://www.awesomestore.com.gh/logo.png'
    },
    opts: {
      showFloatingButton: true,
      floatingButtonPosition: 'top-right',
      customButton: true
    }
  }).init()
})()

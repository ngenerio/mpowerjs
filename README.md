## MPOWERJS

This is mpowerjs, the browser client library that makes interfacing with mpower as easy as possible.

### Development

To develop or contribute to it, you need node.js particularly **`io.js~2.2.*`**.

Install browserify, babel and gulp as global tools

```bash
npm i -g browserify babel gulp bower
```

Step into the mpower.js folder and install its dependencies by running

```bash
npm i
bower i
```

To develop and checkout your changes as immediately as possible, there is a samples folder that contains the mpower.js built code. To checkout the changes as you develop there is a gulp task for that : just run `gulp`

```bash
gulp
```

After you've successfully contributed to the repo and tested the changes, you need to build the code for production. Run `gulp prod`

```bash
gulp prod
```

**Note**, the javascript code is mainly in es6, so you should know the syntax and semantics of es6 to contribute or develop.

mpower.js uses [JavaScript Standard Style](https://github.com/feross/standard)

[![js-standard-style](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

You should check that one too. It helps you to understand the coding style emplpoyed in mpower.js.

### USAGE

mpower.js is very simple to use. All you need is your public, private and master keys from mpower payments integration setup and your store information.

There is a sample running at [mpower-smaple](http://mpowerjs.surge.sh).

All you need to do in your javascript file on your website is as simple as this:

The `setup` options contains the integration setup `keys` you received from the [MPower Site](http://http://mpowerpayments.com/).

The `store` contains the information about your store that would that would be used on the mpower checkout receipt.

#### OPTS

The `opts` contains information that will used with the ui:

+ The `showFloatingButton:boolean` will determine whether the floating mpower button will be shown.

+ The `floatingButtonPosition` will determing where on your page that floating putton will be posistioned. It's a string and its value is a dash `-` separated one.
    ```js
    // possible values
    'top-right'
    'top-left'
    'bottom-right'
    'bottom-left'
    ```

+ The last option in the `opts` is `customButton`. The `customButton` will tell mpower.js to look for an element on the page with a `classname` of `mp-view-cart`. It will then add a click event listener to that element. When the element is clicked, the mpower widget view is shown.

For a running example, checkout [this](http://mpowerjs.surge.sh)


```javascript
new MPWidget({
  setup: {
    masterKey: <MASTER_KEY>,
    privateKey: <PRIVATE_KEY>,
    publicKey: <PUBLIC_KEY>,
    token: <TOKEN>,
    mode: <MODE> ('live', 'test')
  },
  store: {
    name: 'Awesome Store', // only name is required
    tagline: 'Easy shopping',
    phoneNumber: '0270000000',
    postalAddress: 'P.0. Box MP555, Accra',
    logoURL: 'http://www.awesomestore.com.gh/logo.png'
  },
  opts: {
    showFloatingButton: true,
    floatingButtonPosition: 'top-right',
    customButton: true
  }
}).init()
```

Before it works on your page, you have to declare all your elements in a particular format. Any item on your page that is part of your products must be declared in this format

```html
  <div class="mp-cart-items" data-name="Yam phone" data-description="Drop that Yam" data-price="50">
    <p>Yam phone</p>
    <button class="mp-add-cart">Add to Cart</button>
  </div>
```

The `classname`, `mp-cart-items` on the root node is very important as well as the `data` attributes, mainly; `data-name`, `data-price` and `data-description`.

Without them mpower.js will have no idea of figuring out about your cart items and will not be able to function properly.

Add as many as those items on your page and leave the rest to mpower.js.


By MPower developers with ❤.

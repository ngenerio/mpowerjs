class Product {
  constructor (opts) {
    let data = {
      name: opts.name,
      price: opts.price,
      description: opts.description,
      currencyCode: opts.currencyCode,
      quantity: opts.quantity,
      total: 0
    }
    this._data  = data
    this.amount = 0
    this._discount = opts.discountPercentage
    this.uid = this.generateProductHash()

    this.calculateTotal()
  }

  generateProductHash () {
    let hash = (Math.random() * Date.now()) | 0
    hash = hash < 0 ? -hash : hash

    return 'mp-' + hash
  }

  getVal (key) {
    let val = this._data[key]
    if (typeof val === 'number') return val
    return val || this._data
  }

  setVal (key, value) {
    if (typeof key === 'object') {
      for (let prop of Object.keys(key)) {
        this._data[prop] = key[prop]
        if (prop === 'quantity' || prop === 'price') {
          this.calculateTotal()
        }
      }
      return
    }

    this._data[key] = value
    if (key === 'quantity' || key === 'price') {
      this.calculateTotal()
    }
  }

  calculateDiscount () {
    let discountPercentage = this._discount
    let amount = this.calculateAmount()

    return discountPercentage * amount
  }

  calculateTotal () {
    let total = this.calculateAmount() - this.calculateDiscount()
    this.setVal('total', total)
    return total
  }

  calculateAmount () {
    let qty = this.getVal('quantity')
    let price = this.getVal('price')
    let amount = qty * price
    this.amount = amount
    return amount
  }
}

export default Product

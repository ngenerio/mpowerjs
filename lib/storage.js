class Storage {
  constructor (name) {
    if (!name) {
      throw new Error('Provide a name for localStorage')
    }

    this._name = name
  }

  save (data) {
    let jsonString = JSON.stringify(data)
    localStorage.setItem(this._name, jsonString)
  }

  retrieve () {
    let jsonString = localStorage.getItem(this._item)
    return JSON.parse(jsonString)
  }

  reset () {
    localStorage.removeItem(this._name)
  }
}

export default Storage

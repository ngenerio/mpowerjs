import {EventEmitter} from 'events'
import Product from './product'
import Storage from './storage'
import mpower from 'mpower'

class Cart extends EventEmitter {
  constructor (opts) {
    super()
    let self = this
    this.total = 0
    self._productItems = []
    self.settings = {
      business: opts.store.phoneNumber,
      name: opts.store.name,
      description: opts.store.tagline
    }
    self.invoice = new mpower.CheckoutInvoice(new mpower.Setup(opts.setup), new mpower.Store(opts.store))
    self.store = new Storage(self.settings.name)

    self.on('remove', function (uid) {
      let i = 0
      for (let product of self._productItems) {
        if (product.uid === uid)  {
          self._productItems.splice(i, 1)
          return
        }
        i++
      }
    })

    self.on('change', function (uid, qty) {
      for (let product of self._productItems) {
        if (product.uid === uid) {
          product.setVal('quantity', qty)
          return
        }
      }
    })

    self.on('remove', this.postEventsFn)
    self.on('change', this.postEventsFn)
  }

   postEventsFn () {
    this.calculateTotal()
    this.save()
  }

  save () {
    var data = []

    for (var product of this._productItems) {
      data.push(product.getVal())
    }

    this.store.save({items: data})
  }

  add (data) {
    data = new Product(data)
    this._productItems.push(data)
    this.postEventsFn()
    return data
  }

  getProduct (uid) {
    let emptyObj = {}
    let i = 0
    if (!uid) return emptyObj

    for (let product of this._productItems) {
      if (product.uid === uid) {
        return product
      }
      i++
    }

    return emptyObj
  }

  getProductByName (name) {
    for (let product of this._productItems) {
      if (product.getVal('name') === name) return product
    }

    return null
  }

  remove (uid) {
    let i = 0
    for (let product of this._productItems) {
      if (uid === product.uid) {
        this._productItems.splice(i, 1)
        return true
      }
      i++
    }

    return false
  }

  reset () {
    this._productItems = []
    this.store.reset()
  }

  isCartEmpty () {
    return !Boolean(this._productItems.length)
  }

  calculateTotal () {
    let subTotal = 0

    for (let product of this._productItems) {
      subTotal += product.getVal('total')
    }

    this.total = subTotal
    return subTotal
  }

  performCheckout (successCallback, errorCallback) {
    let self = this
    if (self.total <= 0) {
      cb(new Error('The items list is less than zero'), null)
      return
    }

    self.invoice.items = {}
    self.invoice.taxes = {}
    for (let product of self._productItems) {
      if (product.getVal('quantity') === 0) continue
      self.invoice.addItem(product.getVal('name'), product.getVal('quantity'), product.getVal('price'), product.getVal('total'), product.getVal('description'))
    }

    self.invoice.totalAmount = self.total
    self.invoice.description = self.settings.description

    self.invoice.create()
      .then(function () {
        successCallback({
          token: self.invoice.token,
          responseText: self.invoice.responseText,
          url: self.invoice.url
        })
      })
      .catch(function (err) {
        errorCallback(err)
      })
  }
}

export default Cart

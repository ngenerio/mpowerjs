import $ from 'jquery'
import nanoScroller from '../vendor/nanoscroller'

class View {
  constructor (opts) {
    let template = `
      <div class="mp-float-show">
        <img src="images/float-orange.png" alt="float white" />
      </div>
      <div class="mp-widget-view nano">
        <div class="mp-widget-holder nano-content">
          <div class="mp-widget-header">
            <div class="mp-widget-close mp-widget-icon">
              <img src="images/whitex.png" alt="MPOWER LOGO" class="mp-widget-close-icon"/>
            </div>
            <h1 class="mp-widget-header-name"><img src="images/mpowerlogo.png" alt="MPOWER LOGO" /></h1>
          </div>
          <div class="mp-widget-cart-items">
            <div class="mp-message-box"></div>
            <ul class="mp-widget-cart-items-top">
            </ul>
          </div>
          <div class="mp-cart-total">Total: <span class="mp-total-price"></span></div>
          <div class="mp-widget-checkout-button">
            <button class="mp-widget-button">Checkout with MPower</button>
          </div>
        </div>
      </div>
    `

    let css = `
    <style>
        /** initial setup **/
        .nano {
          position : relative;
          width    : 100%;
          height   : 100%;
          overflow : hidden;
        }
        .nano > .nano-content {
          position      : absolute;
          overflow      : scroll;
          overflow-x    : hidden;
          top           : 0;
          right         : 0;
          bottom        : 0;
          left          : 0;
        }
        .nano > .nano-content:focus {
          outline: thin dotted;
        }
        .nano > .nano-content::-webkit-scrollbar {
          display: none;
        }
        .has-scrollbar > .nano-content::-webkit-scrollbar {
          display: block;
        }
        .nano > .nano-pane {
          background : rgba(0,0,0,.25);
          position   : absolute;
          width      : 10px;
          right      : 0;
          top        : 0;
          bottom     : 0;
          visibility : hidden\9; /* Target only IE7 and IE8 with this hack */
          opacity    : .01;
          -webkit-transition    : .2s;
          -moz-transition       : .2s;
          -o-transition         : .2s;
          transition            : .2s;
          -moz-border-radius    : 5px;
          -webkit-border-radius : 5px;
          border-radius         : 5px;
        }

        .mp-float-show {
          position: fixed;
          top: 30px;
          right: 30px;
          background-color: white;
          width: 50px;
          height: 50px;
          text-align: center;
          border-radius: 50%;
          padding: 15px;
          transition: all 0.3s;
          display: none;
          border: 1px solid  rgb(211, 211, 211);
          background: rgb(235, 235, 235);
          cursor: pointer;
        }

        [class^="mp"] {
          font-family: Segoe, 'Segoe UI', 'Helvetica Neue', Arial, sans-serif;
        }

        .mp-float-show img {
          margin-top: 10px;
          margin-left: -5px;
          width: 30px;
          height: 30px;
        }

        .mp-float-show:hover {
          box-shadow: 1px 1px 1px 0px rgba(175, 175, 175, 0.2);
          font-weight: lighter;
        }

        button {
          border: none;
        }

        .nano > .nano-pane > .nano-slider {
          background: #444;
          background: rgba(0,0,0,.5);
          position              : relative;
          margin                : 0 1px;
          -moz-border-radius    : 3px;
          -webkit-border-radius : 3px;
          border-radius         : 3px;
        }
        .nano:hover > .nano-pane, .nano-pane.active, .nano-pane.flashed {
          visibility : visible\9; /* Target only IE7 and IE8 with this hack */
          opacity    : 0.99;
        }
       .nano .nano-slider {
          background: rgb(241, 126, 33);
        }
       .mp-widget-view {
        position: fixed;
        width: 430px;
        right: 0px;
        top: 0px;
        height: 100%;
        text-align: center;
        z-index: 1000;
        background-color: rgb(243, 243, 243);
        padding: 0;
        margin: 0;
        visibility: hidden;
      }

      .mp-widget-icon img {
        width: 20px;
        height: 20px;
      }

      h1, h2, h3, h4, h5, h6 {
        font-weight: lighter;
      }

      .mp-widget-holder {
        height: 100%;
        margin: 0px;
        padding: 0;
        margin-top: -20px;
        text-align: center;
        border: 1px solid rgb(211,211,211);
      }

      @media (max-width: 340px) {
        .mp-item-qty-reduce {
          display: none;
        }

        .mp-item-qty-increase {
          display: none;
        }
      }

      @media (max-width: 600px) {
        .mp-widget-view {
          width: 90%;
        }
        .mp-widget-holder {
          width: 100%;
        }

        .mp-item-total {
          margin-left: 10px;
          margin-right: 10px;
        }

        .mp-item-name {
          margin-left: 2px;
          margin-right: 2px;
          width: 20px;
          white-space: no-wrap;
          text-overflow: ellipsis;
        }

        .mp-item {
          margin: 0px 10px;
        }

        .mp-widget-checkout-button {
          margin: 0px 10px;
        }

        .mp-message-box {
          margin: 30px 10px;
        }
      }

      @media (min-width: 600px) {
        .mp-widget-holder {
          width: 450px;
        }
        .mp-item-total {
          float: right;
          margin-right: 10px;
          margin-left: 10px;
        }

        .mp-item-name {
          margin-left: 10px;
          margin-right: 10px;
        }

        .mp-item {
          margin: 0px 30px;
        }

        .mp-widget-checkout-button {
          margin: 0px 30px;
        }

        .mp-message-box {
          margin: 30px;
        }
      }

      .mp-widget-header {
        background-color: rgb(235, 235, 235);
        position: relative;
      }

      .mp-widget-header-name {
        padding: 30px 0px;
      }

      .mp-widget-icon {
        width: 29.3px;
        height: 20.3px;
      }

      .mp-widget-close {
        position: absolute;
        top: 15px;
        right: 20px;
      }

      .mp-message-box {
        display: none;
      }

      .mp-widget-cart-items-top {
        list-style-type: none;
        padding: 0;
        margin: 0;
      }

      .mp-item {
        padding: 10px;
        padding: 40px 0px;
        border-bottom: 1px solid rgb(175,175,175);
        text-align: left;
      }

      .mp-item:last-child {
        margin-bottom: 0;
      }

      .mp-item button {
        padding: 5px;
        width: 30px;
        background-color: white;
        border-top: 1px solid rgb(175, 175, 175);
        border-bottom: 1px solid rgb(175, 175, 175);
      }

      .mp-item-qty-reduce {
        margin-left: -5px;
        border-right: 1px solid rgb(175,175,175);
      }

      .mp-item-qty-increase {
        margin-right: -5px;
        border-left: 1px solid rgb(175,175,175);
      }

      .mp-item-remove:hover, .mp-widget-close:hover {
        cursor: pointer;
      }

      .mp-item-remove {
        float: right;
        margin-top: 1px;
      }

      .mp-item-qty-input {
        width: 20px;
        padding: 5px;
        background-color: rgb(235, 235, 235);
        border: 1px solid rgb(175, 175, 175);
        text-align: center;
      }

      .mp-item-total {
        float: right;
      }

      .mp-widget-checkout-button .mp-widget-button {
        padding: 20px;
        text-align: center;
        background-color: rgb(241, 126, 33);
        border-radius: 5px;
        color: white !important;
        font-weight: bolder;
        width: 100%;
        border: none;
        font-size: 120%;
        font-weight: lighter;
      }

      .mp-cart-total {
        margin: 20px 0px;
        padding: 20px;
        text-align: center;
      }

      .mp-message-box {
        background-color: white;
        padding: 20px;
        border-radius: 5px;
        text-align: center;
        color: rgb(241, 126, 33);
      }

      .mp-total-price {
        color: rgb(241, 126, 33);
      }

      @keyframes slideInRight {
        0% {
          transform: translate3d(100%, 0, 0);
          visibility: visible;
        }

        100% {
          transform: translate3d(0, 0, 0);
        }
      }

      .slideInRight {
        animation: slideInRight 0.2s ease-in;
      }

      @keyframes slideOutRight {
        0% {
          transform: translate3d(0, 0, 0);
        }

        100% {
          visibility: hidden;
          transform: translate3d(100%, 0, 0);
        }
      }

      .slideOutRight {
        animation: slideOutRight 0.2s ease-in;
      }
    </style>
    `

    $('head').append(css)
    $('body').append(template)

    this.model = opts.model
    this.showing = false
    this.$el = $('.mp-widget-view')
    this.$closeBut = this.$el.find('.mp-widget-close')
    this.$cartItems = this.$el.find('.mp-widget-cart-items-top')
    this.$checkoutButton = this.$el.find('.mp-widget-checkout-button')
    this.$button = this.$checkoutButton.find('button')
    this.$messageBox = this.$el.find('.mp-message-box')
    this.$totalDiv = this.$el.find('.mp-cart-total')
    this.messageBoxClear

    this.$el.nanoScroller()
    this.hide()
    this.subDomEvents()
  }

  subDomEvents () {
    this.$closeBut.on('click', () => this.hide())

    this.$checkoutButton.on('click', 'button',  () => {
      if (this.messageBoxClear) {
        clearTimeout(this.messageBoxClear)
        this.$messageBox.hide()
      }

      if (this.model.isCartEmpty()) {
        this.$messageBox.text('Cart is Empty')
        return
      }

      this.$button.html('Checking out with mpower')
      this.$button.attr('disabled', 'disabled')
      this.model.performCheckout(function (responseBody) {
        location.href = responseBody.url
      }, (err) => {
        if (err.crossDomain) {
          this.$messageBox.text('Cannot connect to MPower. Try again')
        } else {
          this.$messageBox.text('Error checking out with MPower. Try again')
        }

        this.$messageBox.show()
        this.messageBoxClear = setTimeout(() => {
          this.$messageBox.hide()
        }, 5000)
        this.$button.html('Checkout with mpower')
        this.$button.attr('disabled', false)
      })
    })
  }

  subDomEventsForPrdct (uid) {
    var self = this
    let $productEl = this.$cartItems.find(`[data-uid="${uid}"]`)

    let updateQuantity = (subOrAdd, event) => {
      let valueOfQty = Number($productEl.find('input.mp-item-qty-input').val())
      if ((typeof subOrAdd !== '+' && typeof subOrAdd !== '-') && event.keyCode === 8) {
        return
      }
      if (typeof valueOfQty === 'number' && valueOfQty <= 0 && subOrAdd === '-') {
        return
      }
      if (typeof valueOfQty !== 'number' || valueOfQty !== valueOfQty) {
        valueOfQty = 0
      } else {
        valueOfQty = typeof subOrAdd === 'number' ? valueOfQty : (subOrAdd === '-' ? valueOfQty - 1 : valueOfQty + 1)
      }

      this.model.emit('change', uid, valueOfQty)
      $productEl.find('input.mp-item-qty-input').val(valueOfQty)
      $productEl.find('.mp-total-actual').text(this.model.getProduct(uid).getVal('total').toLocaleString())
      this.$totalDiv.find('.mp-total-price').text('GHS ' + this.model.total.toLocaleString())
    }


    $productEl.on('click', '.mp-item-remove', () => {
      $productEl.remove()
      this.model.emit('remove', uid)

      if (this.model.isCartEmpty()) {
        this.$messageBox.text('Cart is empty')
        this.$messageBox.show()
        this.$totalDiv.hide()
      }

      this.$totalDiv.find('.mp-total-price').text('GHS ' + this.model.total.toLocaleString())
      this.$el.nanoScroller()
    })

    $productEl.find('input.mp-item-qty-input').on('blur keyup', function (evt) {
      updateQuantity(Number($productEl.find('input.mp-item-qty-input').val()), evt)
    })

    $productEl.find('.mp-item-qty-reduce').on('click', function (evt) { updateQuantity('-', evt)
    })

    $productEl.find('.mp-item-qty-increase').on('click', function (evt) { updateQuantity('+', evt)
    })
  }

  hide () {
    this.$el.addClass('slideOutRight')
    setTimeout(() => {
      this.$el.css('visibility', 'hidden')
      this.$el.removeClass('slideOutRight')
    }, 200)
    this.showing = false
  }

  show () {
    if (this.model.isCartEmpty()) {
      this.$cartItems.hide()
      this.$totalDiv.hide()
      this.$messageBox.text('No items')
      this.$messageBox.show()
    } else {
      this.$cartItems.show()
      this.$totalDiv.show()
      this.$messageBox.hide()
    }

    if (this.showing) return

    this.$el.addClass('slideInRight')
    setTimeout(() => {
      this.$el.css('visibility', 'visible')
      this.$el.removeClass('slideInRight')
    }, 200)
    this.showing = true
  }

  showItems () {
    if (this.$cartItems.css('display') === 'none') {
      this.$cartItems.show()
      this.$totalDiv.show()
    }

    if (this.$messageBox.css('display') === 'block') {
      this.$messageBox.hide()
    }
  }

  addItem (product) {
    let productName = product.getVal('name')
    let quantity = product.getVal('quantity')
    let total = product.getVal('total')

    productName = productName.length > 15 ? productName.substr(0, 13) + '...' : productName

    let template = `
      <li class="mp-item" data-uid="${product.uid}">
        <span class="mp-item-remove mp-widget-icon"><img src="images/orangex.png" alt="remove" /></span>
        <span class="mp-item-total">
          ${product.getVal('currencyCode')}
          <span class="mp-total-actual">${total.toLocaleString()}</span>
        </span>
        <button class="mp-item-qty-increase">+</button>
        <span class="mp-item-qty">
          <input type="text" class="mp-item-qty-input" value="${quantity}"/>
        </span>
        <button class="mp-item-qty-reduce">-</button>
        <span class="mp-item-name">${productName}</span>
      </li>
    `
    this.$cartItems.append(template)
    this.$totalDiv.find('.mp-total-price').text('GHS ' + this.model.total.toLocaleString())

    if (!this.model.isCartEmpty()) {
      this.$totalDiv.show()
    }

    this.showItems()
    this.subDomEventsForPrdct(product.uid)
    this.$el.nanoScroller()
  }

  updateItem (uid, data) {
    let $productEl = this.$cartItems.find(`[data-uid="${uid}"]`)

    this.showItems()
    if (data.quantity) {
      $productEl.find('.mp-item-qty-input').val(data.quantity)
    }

    if (data.total) {
      $productEl.find('.mp-total-actual').text(data.total.toLocaleString())
    }

    this.$totalDiv.find('.mp-total-price').text('GHS ' + this.model.total.toLocaleString())
  }
}

export default View

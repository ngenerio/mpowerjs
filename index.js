import $ from 'jquery'
import Storage from './lib/storage'
import Cart from './lib/cart'
import Product from './lib/product'
import View from './lib/view'

class MPWidget {
  static $(...args) {
    return $.apply(null, args)
  }

  constructor (opts) {
    // opts.el can be a dom selector string or dom element
    this.currencyCode = 'GHS'
    this.$cartNodes = $('.mp-cart-items')
    this.model = new Cart({
      store: opts.store,
      setup: opts.setup
    })
    this.mpView = new View({
      model: this.model
    })
    this.opts = opts.opts
  }

  init () {
    // preload the images to make the UX better
    for (let img of ['mpowerlogo', 'orangex', 'whitex']) {
      let imgObject = new Image()
      imgObject.src = 'images/' + img + '.png'
    }
    this.bindEvents()
  }

  bindEvents () {
    var self = this
    this.$cartNodes.each(function (index, elem) {
      let $elem = $(elem)
      let name = $elem.attr('data-name')
      let price = Number($elem.attr('data-price'))
      let description = $elem.attr('data-description')
      let currencyCode = self.currencyCode

      $elem.on('click', '.mp-add-cart', function () {

        if (!self.mpView.showing) self.mpView.show()

        let product = self.model.getProductByName(name)

        if (product) {
          self.model.emit('change', product.uid, product.getVal('quantity') + 1)
          self.mpView.updateItem(product.uid, {
            quantity: product.getVal('quantity'),
            total: product.getVal('total')
          })
        } else {
          let data = self.model.add({
            name: name,
            price: price,
            quantity: 1,
            currencyCode: currencyCode,
            discountPercentage: 0,
            description: description
          })
          self.mpView.addItem(data)
        }
      })
    })

    if (this.opts.customButton) {
      $('.mp-view-cart').on('click', () => {
        this.mpView.show()
      })
    }

    if (this.opts.showFloatingButton) {
      let $floatingButton = $('.mp-float-show')
      let position = this.opts.floatingButtonPosition.split('-')
      $floatingButton.css(position[0], '30px')
      $floatingButton.css(position[1], '30px')

      $floatingButton.show()
      $floatingButton.on('click', () => this.mpView.show())
    }

    let regExp = /^\.mp-(.*?)/
    $('body').on('click', (evt) => {
      let $elem = $(evt.target)
      let className = $elem.attr('class')

      if (!className) return

      className = '.' + className.split(' ')[0]

      let match = className.match(regExp)
      if (this.mpView.$el.find(className).length || (match && match.length)) return

      this.mpView.hide()
    })
  }
}


export default MPWidget

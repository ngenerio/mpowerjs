var gulp = require('gulp')
var source = require('vinyl-source-stream')
var browserify = require('browserify')
var uglifyify = require('uglifyify')
var watchify = require('watchify')
var babel = require('babelify')
var $ = require('gulp-load-plugins')()
var del = require('del')
var browserSync = require('browser-sync')

require('shelljs/global')

var reload = browserSync.reload

const VERSION = require('./package.json').version

function globalJsBuild(opts) {
  var bundler = browserify('index.js', {
      debug: false,
      standalone: 'MPWidget'
    })
    .transform(babel)

    if (opts.global) bundler = bundler.transform(uglifyify, opts)

  bundler.bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    .pipe(source('index.js'))
    .pipe($.rename(opts.global ? 'mpower.min.js' : 'mpower.js'))
    .pipe(gulp.dest('dist'))
}

// All production tasks are defined here

gulp.task('build-js', function () { return globalJsBuild({global: true}) })

gulp.task('copy-js', function () {
  return gulp.src('dist/mpower.min.js')
    .pipe($.header(`
      /**
       * Copyright © 2015 MPowerPayments (https://mpowerpayments.com)
       * All rights reserved
       * VERION: ${VERSION}
       */
    `))
    .pipe(gulp.dest('build/js'))
    .pipe($.size({title: 'js'}))
})

gulp.task('copy-images', function () {
  return gulp.src('sample/images/**')
    .pipe(gulp.dest('build/images'))
    .pipe($.size({title: 'images'}))
})

gulp.task('prod', ['build-js', 'copy-js', 'copy-images'])

// All development tasks are defined here
gulp.task('build-dev', function () { return globalJsBuild({global: false}) })

gulp.task('copy-dev', function () {
  return gulp.src('dist/mpower.js')
    .pipe(gulp.dest('sample/js'))
    .pipe($.size({title: 'dev-js'}))
})

gulp.task('watch', function () {
  gulp.watch(['index.js', 'lib/**/*.js'], ['build-dev', 'copy-dev'])
})

gulp.task('serve', function () {
  gulp.task('serve', function () {
    browserSync({
      notify: false,
      logPrefix: 'MPOWERJS',
      server: ['sample']
    })

    gulp.watch(['sample/index.html'], reload);
    gulp.watch(['sample/css/**/*.css'], reload);
    gulp.watch(['sample/js/**/*.js'], reload);
    gulp.watch(['sample/images/**/*'], reload);
  })
})

gulp.task('default', ['build-dev', 'copy-dev', 'watch', 'serve'])

gulp.task('deploy', function () {
  exec('surge --domain mpowerjs.surge.sh --project sample', function (code, output) {
    if (code === 0) {
      echo("Sucessfully deployed to http://mpowerjs.surge.sh")
      return exit(code)
    }

    exit(1)
  })
})
